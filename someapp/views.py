from django.shortcuts import render
from django.contrib.auth.models import User


def home(request):
    """View to display home page

    Args:
        request (HttpRequest): Django HttpRequest object

    Returns:
        response: HTTP response
    """
    return render(request, "home.html")


def users(request):
    users = User.objects.all()

    return render(request, "users.html", {"users": users})

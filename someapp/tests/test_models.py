from someapp.models import Thing


def test_thing_model_str_method():
    """ Tests '__str__' method of Thing model """
    thing = Thing(name='somename')
    assert 'somename' in str(thing)

from io import StringIO

from django.core.management import call_command
from unittest.mock import patch


@patch("someapp.management.commands.seed.mixer")
def test_seed_command(mock_mixer):
    """ Checks seed command works as expected """
    out = StringIO()
    call_command("seed", stdout=out)

    assert "Done" in out.getvalue()
    assert mock_mixer.cycle.call_count == 2
    mock_mixer.cycle.assert_called()

def test_ajax_request(rf):
    """ Tests 'request.is_ajax()' functionality of Django request """
    request = rf.get("", **{"HTTP_X_REQUESTED_WITH": "XMLHttpRequest"})
    assert request.is_ajax() is True

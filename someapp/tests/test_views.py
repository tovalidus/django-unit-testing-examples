from unittest.mock import patch

from django.contrib.auth.models import User
from django.urls import reverse
from mixer.backend.django import Mixer

from someapp.views import home, users

mixer = Mixer(commit=False)


def test_home_view(rf):
    """Tests home view

    Args:
        rf (RequestFactory): Django Request Factory object
    """
    request = rf.get(reverse("home"))
    response = home(request)

    assert response.status_code == 200
    assert "app" in response.content.decode().lower()


@patch("someapp.views.User", autospec=True)
def test_users_view(mock_user, rf):
    test_users = mixer.cycle(2).blend(User)
    mock_user.objects.all.return_value = test_users

    request = rf.get(reverse("users"))
    response = users(request)

    assert response.status_code == 200
    assert test_users[0].first_name in response.content.decode()
    mock_user.objects.all.assert_called()

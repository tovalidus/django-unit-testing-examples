from datetime import date
from unittest import mock

import requests
from django.utils import timezone
import pytest

from someapp.utils import get_date, get_datetime, get_posts


@mock.patch(
    "someapp.utils.timezone.now", autospec=True,
)
def test_get_datetime(mock_now):
    custom_now = timezone.datetime(1950, 3, 22)
    mock_now.return_value = custom_now

    assert timezone.now() == custom_now
    assert get_datetime() == custom_now
    mock_now.assert_called()


@mock.patch("someapp.utils.date", autospec=True)
def test_get_date(mock_date):
    custom_date = date(2000, 5, 16)
    mock_date.return_value = custom_date

    assert get_date() == custom_date
    mock_date.assert_called_once_with(2019, 3, 22)


@mock.patch("someapp.utils.requests.get", autospec=True)
def test_get_posts(mock_get):

    mock_get.return_value.json.return_value = [
        {"id": 1, "body": "Post 1"},
        {"id": 2, "body": "Post 2"},
    ]

    posts = get_posts()

    assert isinstance(posts[0], dict)
    assert "body" in posts[0].keys()
    mock_get.assert_called_with("https://jsonplaceholder.typicode.com/posts")
    mock_get.assert_called_once()


@mock.patch("someapp.utils.requests.get", autospec=True)
def test_get_posts_raises_exception(mock_get):
    mock_get.side_effect = requests.exceptions.RequestException()

    with pytest.raises(requests.exceptions.RequestException):
        get_posts()

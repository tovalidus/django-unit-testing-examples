from rest_framework import routers
from someapp.api.viewsets import ThingViewSet

router = routers.DefaultRouter()
router.register("things", ThingViewSet, basename="thing")

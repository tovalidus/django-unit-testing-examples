from django.urls import path, include
from someapp.api.router import router

urlpatterns = [path("", include(router.urls))]

from rest_framework.viewsets import ModelViewSet
from someapp.models import Thing
from someapp.api.serializers import ThingSerializer


class ThingViewSet(ModelViewSet):
    """
    This endpoint presents Things.
    retrieve:
        Returns an Thing instance
    list:
        Return all Things, ordered by earliest joined
    create:
        Create a new Thing
    delete:
        Remove an existing Thing
    partial_update:
        Update one or more fields on an existing Thing
    update:
        Update an Thing
    """

    queryset = Thing.objects.all()
    serializer_class = ThingSerializer

from rest_framework.serializers import ModelSerializer
from someapp.models import Thing


class ThingSerializer(ModelSerializer):
    class Meta:
        model = Thing
        fields = ("id", "name")

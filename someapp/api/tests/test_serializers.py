import pytest

from someapp.api.serializers import ThingSerializer


@pytest.mark.parametrize(
    "item_id, name, expected", [(1, "A", True), (2, None, False)],
)
def test_thing_serializer_validity(item_id, name, expected):
    data = {
        "id": item_id,
        "name": name,
    }

    serializer = ThingSerializer(data=data)
    assert serializer.is_valid() is expected

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from mixer.backend.django import mixer

from someapp.models import Thing


class Command(BaseCommand):
    """Seeds the database with initial data

    Args:
        BaseCommand (BaseCommand): Django Command object
    """
    help = "Seeds the database with initial data"

    def handle(self, *args, **kwargs):
        mixer.cycle(10).blend(User)
        mixer.cycle(5).blend(Thing, name=mixer.faker.word, owner=mixer.SELECT)
        self.stdout.write("Done seeding.")

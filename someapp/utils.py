from django.utils import timezone
from datetime import date
import requests


def get_datetime():
    return timezone.now()


def get_date():
    return date(2019, 3, 22)


def get_posts():
    r = None

    try:
        r = requests.get("https://jsonplaceholder.typicode.com/posts")
    except requests.exceptions.RequestException:
        raise

    return r.json()

from django.db import models
from django.contrib.auth.models import User


class Thing(models.Model):
    """Model definition for Thing."""

    name = models.CharField(max_length=25)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="things")

    def __str__(self):
        """Unicode representation of Thing."""
        return f"<{self.__class__.__qualname__}: {self.name}>"

    def __repr__(self):
        """Unicode representation of Thing."""
        return str(self)

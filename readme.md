# Django Unit Testing examples

Application to show examples of unit testing in Django.

## Requirements

- Python 3.8
- Django 3.1
- pytest
- pytest-django

## Installation

### Clone Project

```sh
git clone https://tovalidus@bitbucket.org/tovalidus/django-unit-testing-examples.git 
```

and then change to directory

```sh
cd django-unit-testing-examples
```

## Create virtual environment
```sh
python -m venv venv
```

once created, activate environment

```sh
source venv/bin/activate
```

### Install Requirements

```sh
pip install -r requirements.txt
```

### Migrate database tables

```sh
python manage.py migrate
```

### Seed database

To populate database with sample data, run:

```sh
python manage.py seed
```

### Run tests

```sh
pytest -vs
```

### Further Reading

- [https://www.cameronmaske.com/how-to-mock-patch-a-function-testing-python-with-pytest/](https://www.cameronmaske.com/how-to-mock-patch-a-function-testing-python-with-pytest/)
- [https://realpython.com/testing-third-party-apis-with-mocks/](https://realpython.com/testing-third-party-apis-with-mocks/)
- [https://docs.python.org/3/library/unittest.mock.html](https://docs.python.org/3/library/unittest.mock.html)
- [https://matthewdaly.co.uk/blog/2015/08/02/testing-django-views-in-isolation/](https://matthewdaly.co.uk/blog/2015/08/02/testing-django-views-in-isolation/)
- [https://stackoverflow.com/questions/15753390/how-can-i-mock-requests-and-the-response](https://stackoverflow.com/questions/15753390/how-can-i-mock-requests-and-the-response)
- [https://stackoverflow.com/questions/48432029/mocking-requests-post-and-requests-json-decoder-python](https://stackoverflow.com/questions/48432029/mocking-requests-post-and-requests-json-decoder-python)
